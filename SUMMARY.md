# Summary

- [Introduction](README.md)
- [Up & Going](up & going/README.md)
  - [Table of Contents](up & going/toc.md)
  - [Foreword](up & going/foreword.md)
  - [Preface](preface.md)
  - [1: Into Programming](up & going/ch1.md)
  - [2: Into JavaScript](up & going/ch2.md)
  - [3: Into YDKJS](up & going/ch3.md)
  - [A: Thank You's!](up & going/apA.md)
- [Scope & Closures](scope & closures/README.md)
  - [Table of Contents](scope & closures/toc.md)
  - [Foreword](https://shanehudson.net/2014/06/03/foreword-dont-know-js/)
  - [Preface](preface.md)
  - [1: What is Scope?](scope & closures/ch1.md)
  - [2: Lexical Scope](scope & closures/ch2.md)
  - [3: Function vs. Block Scope](scope & closures/ch3.md)
  - [4: Hoisting](scope & closures/ch4.md)
  - [5: Scope Closures](scope & closures/ch5.md)
  - [A: Dynamic Scope](scope & closures/apA.md)
  - [B: Polyfilling Block Scope](scope & closures/apB.md)
  - [C: Lexical-this](scope & closures/apC.md)
  - [D: Thank You's!](scope & closures/apD.md)
- [this & Object Prototypes](this & object prototypes/README.md)
  - [Table of Contents](this & object prototypes/toc.md)
  - [Foreword](this & object prototypes/foreword.md)
  - [Preface](preface.md)
  - [1: -this- Or That?](this & object prototypes/ch1.md)
  - [2: -this- All Makes Sense Now!](this & object prototypes/ch2.md)
  - [3: Objects](this & object prototypes/ch3.md)
  - [4: Mixing (Up) "Class" Objects](this & object prototypes/ch4.md)
  - [5: Prototypes](this & object prototypes/ch5.md)
  - [6: Behavior Delegation](this & object prototypes/ch6.md)
  - [A: ES6 -class-](this & object prototypes/apA.md)
  - [B: Thank You's!](this & object prototypes/apB.md)
- [Types & Grammar](types & grammar/README.md)
  - [Table of Contents](types & grammar/toc.md)
  - [Foreword](types & grammar/foreword.md)
  - [Preface](preface.md)
  - [1: Types](types & grammar/ch1.md)
  - [2: Values](types & grammar/ch2.md)
  - [3: Natives](types & grammar/ch3.md)
  - [4: Coercion](types & grammar/ch4.md)
  - [5: Grammar](types & grammar/ch5.md)
  - [A: Mixed Environment JavaScript](types & grammar/apA.md)
  - [B: Thank You's!](types & grammar/apB.md)
- [Async & Performance](async & performance/README.md)
  - [Table of Contents](async & performance/toc.md)
  - [Foreword](async & performance/foreword.md)
  - [Preface](preface.md)
  - [1: Asynchrony: Now & Later](async & performance/ch1.md)
  - [2: Callbacks](async & performance/ch2.md)
  - [3: Promises](async & performance/ch3.md)
  - [4: Generators](async & performance/ch4.md)
  - [5: Program Performance](async & performance/ch5.md)
  - [6: Benchmarking & Tuning](async & performance/ch6.md)
  - [A: Library: asynquence](async & performance/apA.md)
  - [B: Advanced Async Patterns](async & performance/apB.md)
  - [C: Thank You's!](async & performance/apC.md)
- [ES6 & Beyond](es6 & beyond/README.md)
  - [Table of Contents](es6 & beyond/toc.md)
  - [Foreword](es6 & beyond/foreword.md)
  - [Preface](preface.md)
  - [1: ES? Now & Future](es6 & beyond/ch1.md)
  - [2: Syntax](es6 & beyond/ch2.md)
  - [3: Organization](es6 & beyond/ch3.md)
  - [4: Async Flow Control](es6 & beyond/ch4.md)
  - [5: Collections](es6 & beyond/ch5.md)
  - [6: API Additions](es6 & beyond/ch6.md)
  - [7: Meta Programming](es6 & beyond/ch7.md)
  - [8: Beyond ES6](es6 & beyond/ch8.md)
  - [A: Thank You's!](es6 & beyond/apA.md)
